# SLUT_BME280

Moduł czujnika BME280.



| Model 3D TOP| Model 3D BOTTOM |
--- | ---
|![plot](./PCB_TOP_VIEW.png)|![plot](./PCB_BOTTOM_VIEW.png)|

Datasheet: https://datasheet.octopart.com/BME280-Bosch-datasheet-34148998.pdf